#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_magic.mk

COMMON_LUNCH_CHOICES := \
    lineage_magic-user \
    lineage_magic-userdebug \
    lineage_magic-eng
